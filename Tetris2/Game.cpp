#include <Windows.h>
#include "Game.h"
#include "RectGame.h"

Game* Game::instance = nullptr;

Game::Game()
{
}


Game::~Game()
{
}

Game* Game::GetInstance()
{
	if (!instance)
	{
		instance = new Game();
	}

	return instance;
}

void Game::DeleteInstance()
{
	delete instance;
}


void Game::Init(HWND hWnd)
{
	//activeScene = new GameScene();
	//activeScene->Init();

	//scenes["gameScene"] = activeScene;

	scenes["endScene"] = new EndScene();
	scenes["gameScene"] = new GameScene();

	activeScene = new MainScene();
	activeScene->Init();
	scenes["mainScene"] = activeScene;


	//SetTimer(_hWnd, //������� ����, �������� ������ ����� �������� ���������
	//	1,  //�������������� �������        
	//	100, //������ � ��
	//	0); //������� ���������� 0 - ��� ����������� ��� ����� �������


	//KillTimer(hWnd,1);
	
}

void Game::Draw()
{
	activeScene->Draw();
}

Scene *Game::GetActiveScene()
{
	return activeScene;
}

void Game::Event(GameEvent event)
{
	switch (event)
	{

	case evMainScene:
		activeScene = scenes["mainScene"];
		activeScene->Init();

	case evGameOver:
		activeScene = scenes["endScene"];
		
		break;
	case evGameScene:
		activeScene = scenes["gameScene"];
		activeScene->Init();

		break;
	case evTimer:

		break;

	default: //evRotate: evDown: evLeft: evRight: evLevelUp: break;
		activeScene->Event(event);
		break;
	}
}