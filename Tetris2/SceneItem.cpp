#include "SceneItem.h"


SceneItem::SceneItem():
	isdelete(false)
{
}


SceneItem::~SceneItem()
{
}

void SceneItem::DeleteLater()
{
	isdelete = true;
}

bool SceneItem::IsDelete() const
{
	return isdelete;
}

Size SceneItem::GetSize()
{
	return size;
}

void SceneItem::SetSize(int w, int h)
{
	size.w = w; size.h = h;
}