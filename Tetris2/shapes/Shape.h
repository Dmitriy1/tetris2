#pragma once

#include "../SceneItem.h"
#include "../Types.h"

#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>


const int SHAPE_W = 4; //������ ���� ������
const int SHAPE_H = 4; //������ ���� ������
const int SHAPE_SIZE = SHAPE_H * SHAPE_W;

class Shape
{
public:
	Shape();
	virtual ~Shape();
		

	virtual void Init();
	virtual void Draw();
	virtual void Rotate();
	
	uchar GetPoint(uchar x, uchar y)	{ return rect[size.w * y + x]; }

	Point GetGlobalPosition();
	void SetGlobalPosition(uchar x, uchar y);

	void SetSize(uchar w, uchar h);
	Size GetSize() const	{ return size; }

	Pointf GrPosition() const	{ return grCoor; }
	void SetGrPosition(Pointf coor)	{ grCoor = coor; }
	

	void SetpartSize(Pointf size)	{ partSize = size; }
	Pointf GetpartSize() const	{ return partSize; }
		
	uchar *GetRect();

	void UndoRotate();
	

protected:
	
	void RandRotate(uchar countRotations);

	uchar *rect;
	Point globalCoor; //���������� ���� ������ ������������ ������� �������� ����  (������� ����� ����)
	Pointf grCoor; //����������� ���������� ������� ����� �����
	int sizeMem; // ���������� ������ ����������� ��� ���� ������
	uchar *undoRect;
	Size undoSize;

	Pointf partSize;
private:
	Size size;

	
	
	
};