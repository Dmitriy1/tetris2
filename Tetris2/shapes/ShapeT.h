#pragma once

#include "Shape.h"

class ShapeT :	public Shape
{
public:
	ShapeT();
	virtual ~ShapeT() override;
	void Init();
};

