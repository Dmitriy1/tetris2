#pragma once

#include "Shape.h"

class ShapeI :	public Shape
{
public:
	ShapeI();
	virtual ~ShapeI() override;

	void Init();
};

