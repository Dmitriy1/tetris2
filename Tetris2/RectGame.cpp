#include "RectGame.h"
#include "Types.h"
#include <iostream>
#include <ctime>
#include "Render.h"
#include <string>
#include "Game.h"


Color colors[] = { Color(0.25, 0.25, 0.25), Color(0.4, 0.2, 0.35), Color(0.0, 0.2, 0.7), Color(0.2, 0.5, 0.25), Color(0.3, 0.4, 0.3) };

RectGame::RectGame():
	scope(0),
	activeShape(nullptr)
{
}


RectGame::~RectGame()
{
	if (activeShape)
	{
		delete activeShape;
	}
}

void CALLBACK  TimerProc(HWND hwnd, UINT msg, UINT idTimer, DWORD dwTime)
{
	GAME->Event(evTimer);
	GAME->Event(evDown);
}

void RectGame::Init()
{
	level = 0;
	speed = 700;
	scope = 0;

	SetSize(GAME_RECT_W, GAME_RECT_H);
	memset(&rect, 0, GAME_RECT_W * GAME_RECT_H);
	
	GLsizei w = RENDER->GetSize().w;
	GLsizei h = RENDER->GetSize().h;

	geometry = Geometry(w * 0.3, h * 0.1, w * 0.7, h * 0.9 );

	partSize.x = w * 0.4 / GetSize().w; // ����������� ������ �������� ������
	partSize.y = h * 0.8 / GetSize().h;

	geometry.w = w * 0.3;
	geometry.h = h * 0.8;

	CreateShape();

	KillTimer(NULL, nTimerID);
	
	nTimerID = SetTimer(NULL, 0, speed, (TIMERPROC)TimerProc);

	
}

void RectGame::Draw()
{
	GLsizei w = RENDER->GetSize().w;
	GLsizei h = RENDER->GetSize().h;

	bool sharp = true;

	if (sharp)
	{
		
		GLfloat xb = w * 0.3;
		GLfloat yb =  h * 0.1;
		GLfloat xstep =  w * 0.4 / 10;
		GLfloat ystep =  h * 0.8 / 18;


		glColor3f(0.0, 0.4, 0.0);
		glBegin(GL_LINES);
		for (uchar i = 0; i < GetSize().w; ++i)
		{
			glVertex3f(xb, geometry.y1, 1);
			glVertex3f(xb, geometry.y2, 1);
			xb += xstep;
		}
		glEnd();

		glBegin(GL_LINES);
		for (uchar i = 0; i < GetSize().h; ++i)
		{
			glVertex3f(geometry.x1, yb, 1);
			glVertex3f(geometry.x2, yb, 1);
			yb += ystep;
		}
		glEnd();
	}


	RENDER->DrawRectangle(Point(geometry.x1, geometry.y1), Point(geometry.x2, geometry.y2), 0, Color(0.5, 0.5, 0.5)); //������ �������� ����
	
	if (activeShape)
		activeShape->Draw();


	//������� �� ����
	float gX = geometry.x1;
	float gY = geometry.y1;
	float ofX = w * 0.4 / 10;
	float ofY = h * 0.8 / 18;

	for (int i = 0; i < GetSize().h; ++i)
	{
		for (int j = 0; j < GetSize().w; ++j)
		{
			if (1 == rect[i][j])
			{
				Color smallPart;
				Color bigPart;

				//������ ����� ������� ����� ������� �������
				int colorSize = sizeof(colors) / sizeof(colors[0]);
				int colorId = level % colorSize;

				bigPart = colors[colorId];

				smallPart.r = bigPart.r + 0.05;
				smallPart.g = bigPart.g + 0.05;
				smallPart.b = bigPart.b + 0.05;
				

				RENDER->DrawRectangle(Point(gX + ofX * 0.2, gY + ofY * 0.2), Point(gX + ofX *0.8, gY + ofY * 0.8), 1, bigPart);
				RENDER->DrawRectangle(Point(gX, gY), Point(gX + ofX, gY + ofY), 1, smallPart);
			}
			gX += ofX;
		}
		gX = geometry.x1;
		gY += ofY;
	}


	glColor3f(1.0, 0.0, 0.0);
	RENDER->PrintText(geometry.x2 + RENDER->GetSize().w * 0.05, geometry.y1, "scope:%d", scope);

	glColor3f(0.4, 0.4, 0.4);
	RENDER->PrintText(geometry.x2 + RENDER->GetSize().w * 0.05, geometry.y1+32, "level:%d", level);

}

bool RectGame::CreateShape()
{
	if (activeShape)
	{
		delete activeShape;
		activeShape = nullptr;
	}

	srand(time(0));
	uchar typeShape = rand() % eShapeSize;
	
	switch (typeShape)
	{
		case eShapeI : activeShape = new ShapeI(); //0
			break;
		case eShapeJ: activeShape = new ShapeJ(); //1
			break;
		case eShapeL: activeShape = new ShapeL(); //2
			break;
		case eShapeO: activeShape = new ShapeO(); //3
			break;
		case eShapeS: activeShape = new ShapeS(); //4
			break;
		case eShapeT: activeShape = new ShapeT(); //5
			break;
		case eShapeZ: activeShape = new ShapeZ(); //6
			break;

	default:
		break;
	}

	activeShape->Init();
	activeShape->SetGlobalPosition(3, 0);

	Pointf pp = Pointf(geometry.x1 + 3 * partSize.x, geometry.y1);
	activeShape->SetGrPosition(pp);
	activeShape->SetpartSize(partSize);

	if (IsDownCollision(0, 0))
	{
		return false;
	}
	return true;
}


bool RectGame::IsDownCollision(int ox, int oy)
//ox, oy - ������� ������������ �������� �������� (���� ������ ������ ���� �� ox, oy �����)
{	
	Point p = activeShape->GetGlobalPosition();

	//�������� ������ �� ������� �������� ����
	if (ox < 0)
	{
		if (p.x + ox < 0)
			return true;
	}
	else
		if (ox > 0)
		{
			if (p.x + activeShape->GetSize().w + ox > GAME_RECT_W)
				return true;
		}
		else
			if (oy > 0)
			{
				if (p.y + activeShape->GetSize().h + oy > GAME_RECT_H)
					return true;
			}
			else
				if (p.x + activeShape->GetSize().w > GAME_RECT_W)
					return true;
				else
					if (p.y + activeShape->GetSize().h > GAME_RECT_H)
						return true;
		

	//����������� � ���������� �� ����
	for (uchar i = 0; i < activeShape->GetSize().h; ++i)
	{
		for (uchar j = 0; j < activeShape->GetSize().w; ++j)
		{
			if (1 == activeShape->GetPoint(j,i))
				if (1 == rect[int(p.y) + i + oy][int(p.x) + j + ox])
					return true;
		}
	}


	return false;
	
}


void RectGame::CrashFullLine()
{
	uchar count;
	
	for (uchar i = 0; i < GAME_RECT_H; ++i)
	{
		count = 0;
		for (uchar j = 0; j < GAME_RECT_W; ++j)
		{
			if (1 == rect[i][j])
			{
				++count;
			}
		}
		
		if (GAME_RECT_W == count)
		{
			++scope;
			for (uchar q = i; q > 0; --q)
				memcpy(&rect[q][0], &rect[q-1][0], GAME_RECT_W);

			if (0 == scope % 5)
			{
				++level;
				GAME->Event(evLevelUp);
			}
		}
	}
	
}

void RectGame::AttachShape()
//������������ ������ � ���� ����
{
	Point p = activeShape->GetGlobalPosition();

	for (uchar i = 0; i < activeShape->GetSize().h; ++i)
	{
		for (uchar j = 0; j < activeShape->GetSize().w; ++j)
		{
			rect[int(p.y) + i][int(p.x) + j] |= activeShape->GetPoint(j, i);
		}
	}
}

Shape *RectGame::GetActiveShape()
{
	return activeShape;
}

uchar *RectGame::GetRect()
{
	return (uchar*)&rect;
}

bool RectGame::MoveDownShape()
{
	if (activeShape)
		return MoveShape(0, 1);
	else
		return true;
}

bool RectGame::MoveLeftShape()
{
	return MoveShape(-1,0);
}

bool RectGame::MoveRightShape()
{
	return MoveShape(1, 0);
}


bool RectGame::MoveShape(int x, int y)
{
	if (!IsDownCollision(x, y))
	{
		Point p = activeShape->GetGlobalPosition();
		p.x += x; p.y += y;
		activeShape->SetGlobalPosition(p.x, p.y);

		UpdateActiveShapeGrCoor();
		return true;
	}

	
	return false;
}

void RectGame::Event(GameEvent event)
{

	switch (event)
	{
	case evUp:
	{
		activeShape->Rotate();

		if (IsDownCollision(0, 0))
		{
			// ����� �� ������� ������� ���� ��� ��������
			activeShape->UndoRotate();
		}
		break;
	}

	case evDown:
	{

		bool isDownMove = MoveDownShape();

		if (!isDownMove) //������� ������ ������, ������� �����
		{
			AttachShape();

			bool isCreate = CreateShape();

			CrashFullLine();


			if (!isCreate)
			{
				//game ower !!! 
				KillTimer(NULL, nTimerID);
				GAME->Event(evGameOver);
			}
		};

		break;
	}
	case evLeft:
	{
		MoveLeftShape();
		break;
	}
	case evRight:
	{
		MoveRightShape();
		break;
	}
	case evLevelUp:
	{
		speed -= 20;
		KillTimer(NULL, nTimerID);
		nTimerID = SetTimer(NULL, 0, speed, (TIMERPROC)TimerProc);
	}
	}

}

void RectGame::SetGeometry(Geometry &g)
{
	geometry = g;
}

Geometry RectGame::GetGeometry()
{
	return geometry;
}

void RectGame::UpdateActiveShapeGrCoor()
{
	Point p = activeShape->GetGlobalPosition();
	Pointf newP;
	newP.x = geometry.x1 + p.x * partSize.x;
	newP.y = geometry.y1 + p.y * partSize.y;

	activeShape->SetGrPosition(newP);
}