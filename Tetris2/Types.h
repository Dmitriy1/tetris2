#pragma once


typedef unsigned char uchar;


struct Point
{
	int x;
	int y;

	Point() :x(0), y(0){}
	Point(int xCoor, int yCoor) :x(xCoor), y(yCoor){}
	
//	Point & operator + (Point &a);
};

struct Pointf
{
	float x;
	float y;

	Pointf() :x(0), y(0){}
	Pointf(float xCoor, float yCoor) :x(xCoor), y(yCoor){}

	//	Point & operator + (Point &a);
};



struct Size
{
	int w;
	int h;

	Size() :w(0), h(0){}
	Size(int wSize, int hSize) :w(wSize), h(hSize){}

	//	Point & operator + (Point &a);
};

/*
Point & Point::operator + (Point &a)
{
	x += a.x;
	y += a.y;
	return Point(x,y);
}
*/


struct Color
{
	float r;
	float g;
	float b;

	Color() : r(0), g(0), b(0){}
	Color(float rValue, float gValue, float bValue) : r(rValue), g(gValue), b(bValue){}
};

//static Point gameRectCoor[2]; //���������� ������� �������
//static Point partSize;
//static Point pointSpawn;


struct Geometry
{
	float x1,y1,x2,y2;
	float w, h;

	Geometry() :x1(0), y1(0), x2(0), y2(0), w(0), h(0){}
	Geometry(float x1v, float y1v, float x2v, float y2v) :x1(x1v), y1(y1v), x2(x2v), y2(y2v), w(x2v - x1v), h(y2v - y1v){}
};


enum GameEvent
{
	evRotate = 0,
	evMainScene,
	evGameScene,
	evGameOver,
	evMenuGame,
	evTimer,
	evDown,
	evLeft,
	evUp,
	evRight,
	evLevelUp,
	evStartGame
};