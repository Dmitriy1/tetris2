#include <Windows.h>
#include <gl\GL.h>
#include <gl\GLU.h>

#include "GameScene.h"
#include "..\RectGame.h"



GameScene::GameScene()
{
}


GameScene::~GameScene()
{
}


void GameScene::Init()
{
	auto rg = new RectGame();
	rg->Init();
	sceneItems.push_back(rg);
}

void GameScene::AddItems(SceneItem *itm)
{
	sceneItems.push_back(itm);
}

void GameScene::Draw()
{
	for (auto it = sceneItems.cbegin(); it != sceneItems.cend(); ++it)
	{
		static_cast<SceneItem*>(*it)->Draw();
	}
		
}


std::list<SceneItem *> *GameScene::GetChild()
{
	return &sceneItems;
}
