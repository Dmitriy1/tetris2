#include "Scene.h"


Scene::Scene()
{
}


Scene::~Scene()
{
}

std::list<SceneItem *> *Scene::GetChild()
{
	return &sceneItems;
}

void Scene::AddItems(SceneItem *itm)
{
	sceneItems.push_back(itm);
}

void Scene::Event(GameEvent event)
{
	for (auto it = sceneItems.cbegin(); it != sceneItems.cend(); ++it)
	{
		static_cast<SceneItem*>(*it)->Event(event);
	}
}