#pragma once
#include "Scene.h"
class MainScene : public Scene
{
public:
	MainScene();
	~MainScene();

	void Init();
	void Event(GameEvent event);
	void Draw();

private:

	enum MenuItems
	{
		eStart = 0,
		eAbout,
		eExit,

		MenuSize
	};

	void DrawBtn(Point a, Point b, Color color, char* txt);

	MenuItems stateMenu;

};

