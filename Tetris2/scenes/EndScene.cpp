#include "EndScene.h"
#include "../Render.h"


EndScene::EndScene()
{
}


EndScene::~EndScene()
{
}

void EndScene::Draw()
{
	glColor3f(1.0, 0.0, 0.0);
	RENDER->PrintText(RENDER->GetSize().w * 0.4, RENDER->GetSize().h * 0.5, "GAME OWER");
	glColor3f(0.0, 0.3, 0.2);
	RENDER->PrintText(RENDER->GetSize().w * 0.4, RENDER->GetSize().h * 0.7, "Esc - EXIT");
	RENDER->PrintText(RENDER->GetSize().w * 0.4, RENDER->GetSize().h * 0.75, "F2 - MENU");
}

void EndScene::Event(GameEvent event)
{

}