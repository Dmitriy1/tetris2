#include <stdio.h>

#include "Render.h"
#include "Game.h"

Render *Render::instance = nullptr;

Render::Render()
{
}


Render::~Render()
{
}

Render *Render::GetInstance()
{
	if (!instance)
	{
		instance = new Render();
	}

	return instance;
}

void Render::DeleteInstance()
{
	instance->KillFont();
	delete instance;
}

bool Render::Init(int windowW, int windowH, HDC hdc)// ��� ��������� ������� OpenGL
{
	size.w = windowW; size.h = windowH;
	hDC = hdc;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glShadeModel(GL_SMOOTH);            // ��������� ������� �������� �����������
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glColor3f(0, 0, 0);
	glClearColor(0, 0, 0, 0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, size.w, size.h, 0, -1, 1);
	glMatrixMode(GL_MODELVIEW);

	BuildFont();

	return true;
}


GLvoid Render::PrintText(float x, float y, const char *fmt, ...)     // ������� ������ ������ � OpenGL

{
	
	char    text[256];      // ����� ��� ����� ������
	va_list    ap;          // ��������� �� ������ ����������
	if (fmt == NULL)     // ���� ��� ������
		return;            // ������ �� ������

	va_start(ap, fmt);           // ������ ������ ����������
	vsprintf_s(text, fmt, ap); // � ��������������� �������� � �������� ����
	va_end(ap);                  // ��������� ���������� � ������
	glPushAttrib(GL_LIST_BIT);      // ����������� ���� ������ ����������� ( ����� )
	glListBase(base - 32);          // ������ ���� ������� � 32 ( ����� )

	glRasterPos2f(x,y);

	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);// ����� �������� �����������(�����)
	glPopAttrib(); // ������� ����� ������ ����������� ( ����� )

}

GLvoid Render::BuildFont(GLvoid)  // ���������� ������ ���������� ������
{
	HFONT  font;            // ������������� �����

	base = glGenLists(96);  // ������� ����� ��� 96 �������� ( ����� )
	font = CreateFont(-24,        // ������ ����� ( ����� )
		0,        // ������ �����
		0,        // ���� ���������
		0,        // ���� �������
		FW_BOLD,      // ������ ������
		FALSE,        // ������
		FALSE,        // �������������
		FALSE,        // ��������������
		ANSI_CHARSET,      // ������������� ������ ��������
		OUT_TT_PRECIS,      // �������� ������
		CLIP_DEFAULT_PRECIS,    // �������� ���������
		ANTIALIASED_QUALITY,    // �������� ������
		FF_DONTCARE | DEFAULT_PITCH,  // ��������� � ���
		"Courier New");      // ��� ������
		SelectObject(hDC, font);        // ������� �����, ��������� ���� ( ����� )
		wglUseFontBitmaps(hDC, 32, 96, base); // ��������� 96 �������� ������� � ������� ( ����� )
}


GLvoid Render::KillFont(GLvoid)            // �������� ������
{
	glDeleteLists(base, 96);        // �������� ���� 96 ������� ����������� ( ����� )
}

Size Render::GetSize()
{
	return size;
}

void Render::Draw() //����� ����� ����������� ��� ����������
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity(); // ����� ���������

	GAME->Draw();

	glFlush();
}

void Render::DrawRectangle(const Point &a, const Point &b, const GLfloat z, const Color &color)
{
	glColor3f(color.r, color.g, color.b);    // ������� 
	glBegin(GL_QUADS);
	glVertex3f(a.x, a.y, z);
	glVertex3f(b.x, a.y, z);
	glVertex3f(b.x, b.y, z);
	glVertex3f(a.x, b.y, z);
	glEnd();
}